- think of a channel name
- make a trello board for the channel
- list all type of contents for the channel
- list at least first 5 possible video ideas
- list contents for first 2 videos

### Channel Name
- FullStack guys
- FullStack devs
- FSDevs
- fullStackDevs
- full_stack_devs
- full-stack-devs

### Channel Contents
- Fullstack tools
  - Planning tools
    - Slack
    - Trello
  - Source Control
    - GIT (gitlab)
  - Text Editor
    - VS Code
  - CLI tools
    - zsh
- Frontend
  - HTML
  - CSS
  - JS
  - Bootstrap
  - ReactJs
- Backend
  - Flask
  - Django
  - ExpressJs
- Database
  - MySQL
  - PostgreSQL
  - MongoDB

### First 5 Possible Video Ideas
- **Tools**
  - Planning tools
    - Slack
    - Trello
  - Source Control
    - GIT (gitlab)
  - Text Editor
    - VS Code
  - CLI tools
    - zsh

- **What is FullStack?**
  - Frontend
  - Backend
  - Databases
  - How web works?
  - HTTP

- **What is Frontend?**
  - HTML
  - CSS
  - JS
  - Js Frameworks

- **HTML and CSS**
  - HTML
  - CSS
  - Bootstrap

- **Js**
  - what is JavaScript?
  - Why JavaScript in web?
  - Basics of JavaScript?